-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 07-02-2020 a las 01:47:16
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cloud_tpv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea_pedido`
--

CREATE TABLE `linea_pedido` (
  `id` bigint(20) NOT NULL,
  `cantidad` bigint(45) DEFAULT NULL,
  `precio` int(11) NOT NULL,
  `ticket_id` bigint(20) NOT NULL,
  `producto_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

CREATE TABLE `local` (
  `id` bigint(20) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `nif` varchar(45) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`id`, `descripcion`, `login`, `password`, `nif`, `telefono`, `email`) VALUES
(37, 'Bar Maria', 'biel', 'biel', '37625220O', 676079161, 'Toni5055@cloudtpv.com'),
(38, 'Bar Paquita', 'Maria5639', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '54335580O', 422882176, 'Maria5639@cloudtpv.com'),
(39, 'Bar Maria', 'Toni8780', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '51678873O', 267203320, 'Toni8780@cloudtpv.com'),
(40, 'Restaurante  Paco', 'Paquita5724', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '37469772O', 441150587, 'Paquita5724@cloudtpv.com'),
(41, 'Restaurante  Paco', 'Ramon4587', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '93409133O', 786578437, 'Ramon4587@cloudtpv.com'),
(42, 'Restaurante  Ramon', 'Maria4614', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '23539337O', 905376540, 'Maria4614@cloudtpv.com'),
(43, 'Restaurante  Ramon', 'Toni1448', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '82048498O', 197937647, 'Toni1448@cloudtpv.com'),
(44, 'Bar Toni', 'Maria6719', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '73839440O', 972567136, 'Maria6719@cloudtpv.com'),
(45, 'Pastisseria  Paquita', 'Paco5168', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '96891076O', 264114334, 'Paco5168@cloudtpv.com'),
(46, 'Bar Paco', 'Paquita6193', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '69638846O', 300135296, 'Paquita6193@cloudtpv.com'),
(47, 'Restaurante  Paco', 'Paco2854', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '14064374O', 559061414, 'Paco2854@cloudtpv.com'),
(54, 'Bar Maria', 'Toni1139', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '81921221O', 550817874, 'Toni1139@cloudtpv.com'),
(55, 'Bar Paquita', 'Toni7555', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '13448845O', 114111052, 'Toni7555@cloudtpv.com'),
(56, 'Restaurante  Maria', 'Toni6258', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '67512942O', 743321300, 'Toni6258@cloudtpv.com'),
(57, 'Bar Ramon', 'Paco5508', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '16397173O', 123874251, 'Paco5508@cloudtpv.com'),
(58, 'Bar Paco', 'Paquita6388', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '17394290O', 463964468, 'Paquita6388@cloudtpv.com'),
(59, 'Pastisseria  Maria', 'Maria8916', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '72516849O', 290926853, 'Maria8916@cloudtpv.com'),
(60, 'Restaurante  Toni', 'Ramon5026', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '50404002O', 563843118, 'Ramon5026@cloudtpv.com'),
(61, 'Restaurante  Paco', 'Paco5334', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '82312782O', 178791291, 'Paco5334@cloudtpv.com'),
(62, 'Restaurante  Toni', 'Maria9109', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '53095654O', 648353143, 'Maria9109@cloudtpv.com'),
(63, 'Bar Toni', 'Paquita1363', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '41437907O', 696771185, 'Paquita1363@cloudtpv.com'),
(64, 'Bar Ramon', 'Paco8241', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '53067852O', 570929709, 'Paco8241@cloudtpv.com'),
(65, 'Bar Paquita', 'Toni2942', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '89899876O', 502368222, 'Toni2942@cloudtpv.com'),
(66, 'Pastisseria  Paquita', 'Toni7822', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '80396694O', 251878910, 'Toni7822@cloudtpv.com'),
(67, 'Pastisseria  Ramon', 'Ramon2625', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '52145458O', 793764809, 'Ramon2625@cloudtpv.com'),
(68, 'Bar Paquita', 'Maria8412', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '21566556O', 277223849, 'Maria8412@cloudtpv.com'),
(69, 'Bar Paco', 'Paquita9642', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '64679380O', 296073975, 'Paquita9642@cloudtpv.com'),
(70, 'Pastisseria  Ramon', 'Ramon2330', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '43332326O', 971452971, 'Ramon2330@cloudtpv.com'),
(71, 'Pastisseria  Paco', 'Paco2625', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '86692821O', 754336114, 'Paco2625@cloudtpv.com'),
(72, 'Restaurante  Maria', 'Paquita6796', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '46045924O', 892647493, 'Paquita6796@cloudtpv.com'),
(73, 'Pastisseria  Paquita', 'Maria5124', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '42057557O', 221143594, 'Maria5124@cloudtpv.com'),
(74, 'Pastisseria  Paco', 'Maria5888', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '37141936O', 668680045, 'Maria5888@cloudtpv.com'),
(75, 'Restaurante  Ramon', 'Paquita2518', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '97985124O', 567302256, 'Paquita2518@cloudtpv.com'),
(76, 'Restaurante  Toni', 'Paquita7214', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '55701903O', 479016527, 'Paquita7214@cloudtpv.com'),
(77, 'Bar Paquita', 'Ramon7419', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '66656270O', 750090956, 'Ramon7419@cloudtpv.com'),
(78, 'Bar Ramon', 'Paco9470', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '28303833O', 488117551, 'Paco9470@cloudtpv.com'),
(79, 'Bar Paco', 'Ramon7487', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '11194231O', 845457693, 'Ramon7487@cloudtpv.com'),
(80, 'Bar Ramon', 'Paquita9016', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '34713171O', 253526419, 'Paquita9016@cloudtpv.com'),
(81, 'Pastisseria  Ramon', 'Paquita8392', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '46486375O', 287892977, 'Paquita8392@cloudtpv.com'),
(82, 'Pastisseria  Paquita', 'Paco5157', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '89383548O', 464444679, 'Paco5157@cloudtpv.com'),
(83, 'Pastisseria  Paco', 'Paquita7761', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '88551937O', 964748752, 'Paquita7761@cloudtpv.com'),
(84, 'Restaurante  Ramon', 'Ramon6102', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '21210225O', 514936704, 'Ramon6102@cloudtpv.com'),
(85, 'Bar Paco', 'Ramon6104', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '23409588O', 280423591, 'Ramon6104@cloudtpv.com'),
(86, 'Bar Ramon', 'Ramon2461', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '39516294O', 679869405, 'Ramon2461@cloudtpv.com'),
(87, 'Pastisseria  Ramon', 'Paco7105', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '47716820O', 740111636, 'Paco7105@cloudtpv.com'),
(88, 'Pastisseria  Paco', 'Paco5687', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '28851277O', 441282671, 'Paco5687@cloudtpv.com'),
(89, 'Bar Maria', 'Paco3170', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '48834095O', 902715770, 'Paco3170@cloudtpv.com'),
(90, 'Restaurante  Maria', 'Toni5137', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '58843878O', 508101829, 'Toni5137@cloudtpv.com'),
(91, 'Bar Paco', 'Ramon2768', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '71409958O', 818052655, 'Ramon2768@cloudtpv.com'),
(92, 'Bar Maria', 'Ramon6229', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '98075279O', 482942221, 'Ramon6229@cloudtpv.com'),
(93, 'Bar Maria', 'Paco8984', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '91089255O', 394629236, 'Paco8984@cloudtpv.com'),
(94, 'Restaurante  Ramon', 'Paco3775', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '42726415O', 856221692, 'Paco3775@cloudtpv.com'),
(95, 'Bar Ramon', 'Toni7625', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '73514926O', 341048354, 'Toni7625@cloudtpv.com'),
(96, 'Restaurante  Paquita', 'Maria6699', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '31602384O', 449886151, 'Maria6699@cloudtpv.com'),
(97, 'Restaurante  Ramon', 'Ramon9405', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '18424687O', 986502651, 'Ramon9405@cloudtpv.com'),
(98, 'Restaurante  Paco', 'Paquita1229', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '29049111O', 590768328, 'Paquita1229@cloudtpv.com'),
(99, 'Restaurante  Maria', 'Paquita5008', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '44337312O', 113844016, 'Paquita5008@cloudtpv.com'),
(100, 'Pastisseria  Paco', 'Paco9122', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '56252149O', 566939812, 'Paco9122@cloudtpv.com'),
(101, 'Bar Paquita', 'Paquita9283', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '39079050O', 732367780, 'Paquita9283@cloudtpv.com'),
(102, 'Restaurante  Toni', 'Paquita4680', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '49444473O', 609381113, 'Paquita4680@cloudtpv.com'),
(103, 'Pastisseria  Paquita', 'Paquita3451', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '45219091O', 376728201, 'Paquita3451@cloudtpv.com'),
(104, 'Bar Ramon', 'Paquita1054', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '30649819O', 845884427, 'Paquita1054@cloudtpv.com'),
(105, 'Restaurante  Paco', 'Toni7928', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '67363853O', 743304241, 'Toni7928@cloudtpv.com'),
(106, 'Restaurante  Toni', 'Maria5892', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '72750222O', 894318701, 'Maria5892@cloudtpv.com'),
(107, 'Pastisseria  Paquita', 'Paquita5351', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '20203196O', 965862042, 'Paquita5351@cloudtpv.com'),
(108, 'Bar Paquita', 'Toni1429', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '15890950O', 198226177, 'Toni1429@cloudtpv.com'),
(109, 'Bar Paco', 'Paco8395', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '62207588O', 388512642, 'Paco8395@cloudtpv.com'),
(110, 'Pastisseria  Paquita', 'Ramon4948', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '56523242O', 859184910, 'Ramon4948@cloudtpv.com'),
(111, 'Bar Maria', 'Maria7279', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '19943534O', 615740918, 'Maria7279@cloudtpv.com'),
(112, 'Restaurante  Maria', 'Paco7956', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '53734760O', 987460973, 'Paco7956@cloudtpv.com'),
(113, 'Restaurante  Ramon', 'Maria6190', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '32105493O', 506684343, 'Maria6190@cloudtpv.com'),
(114, 'Pastisseria  Maria', 'Maria4528', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '99929876O', 475189548, 'Maria4528@cloudtpv.com'),
(115, 'Bar Paco', 'Paco3427', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '60555834O', 933866836, 'Paco3427@cloudtpv.com'),
(116, 'Restaurante  Toni', 'Toni5984', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '34396444O', 194663402, 'Toni5984@cloudtpv.com'),
(117, 'Bar Ramon', 'Paco8768', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '87920277O', 245246089, 'Paco8768@cloudtpv.com'),
(118, 'Bar Paquita', 'Paquita7531', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '38832864O', 338272407, 'Paquita7531@cloudtpv.com'),
(119, 'Pastisseria  Paquita', 'Paco6424', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '77604576O', 552709681, 'Paco6424@cloudtpv.com'),
(120, 'Restaurante  Ramon', 'Paco7503', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '63508827O', 346227647, 'Paco7503@cloudtpv.com'),
(121, 'Restaurante  Paquita', 'Toni8925', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '92479110O', 820057401, 'Toni8925@cloudtpv.com'),
(122, 'Pastisseria  Paco', 'Toni3578', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '51395266O', 413418202, 'Toni3578@cloudtpv.com'),
(123, 'Bar Toni', 'Paquita7526', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '52921363O', 739035407, 'Paquita7526@cloudtpv.com'),
(124, 'Bar Paco', 'Pacopacoco', 'ff', '95323621O', 227588722, 'Paco5733@cloudtpv.com'),
(126, 'adsf', 'ss', 'ff', 'f', 4, 'adsf'),
(127, NULL, NULL, NULL, NULL, NULL, NULL),
(128, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 'Sergi Bujias', 'bujias', 'bujias', 'bujiasSL', 666666666, 'sergi@bujiasbujias.com'),
(130, 'f', 'f', 'f', 'f', 2, 'f');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id` bigint(20) NOT NULL,
  `numero` bigint(20) DEFAULT NULL,
  `local_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id`, `numero`, `local_id`) VALUES
(3, 123, 85),
(4, 5, 38),
(11, 1, 129),
(12, 2, 129),
(13, 3, 129),
(14, 3, 129),
(15, 4, 129),
(16, 5, 129),
(17, 6, 129),
(18, 7, 129),
(19, 8, 129),
(20, 9, 129),
(21, 10, 129),
(22, 11, 129),
(23, 12, 129),
(24, 13, 129),
(25, 14, 129),
(26, 15, 129),
(27, 16, 129),
(28, 17, 129),
(49, 1, 37),
(54, 2, 37),
(56, 1, 130),
(57, 2, 130);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` bigint(20) NOT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `tipo_producto_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `codigo`, `precio`, `imagen`, `descripcion`, `tipo_producto_id`) VALUES
(5, '123456', 1.25, 'https://www.myamericanmarket.com/873-large_default/coca-cola-classic.jpg', 'Coca Cola', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id` bigint(20) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `total` double DEFAULT NULL,
  `mesa_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id`, `fecha`, `total`, `mesa_id`) VALUES
(4, '2020-01-09 08:33:08', 27.55, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `id` bigint(20) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_producto`
--

INSERT INTO `tipo_producto` (`id`, `descripcion`) VALUES
(3, '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `linea_pedido`
--
ALTER TABLE `linea_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_linea_pedido_ticket1_idx` (`ticket_id`),
  ADD KEY `fk_linea_pedido_producto1_idx` (`producto_id`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mesa_local1_idx` (`local_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producto_tipo_producto_idx` (`tipo_producto_id`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ticket_mesa1_idx` (`mesa_id`);

--
-- Indices de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `linea_pedido`
--
ALTER TABLE `linea_pedido`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `local`
--
ALTER TABLE `local`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `linea_pedido`
--
ALTER TABLE `linea_pedido`
  ADD CONSTRAINT `fk_linea_pedido_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_linea_pedido_ticket1` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Filtros para la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD CONSTRAINT `fk_mesa_local1` FOREIGN KEY (`local_id`) REFERENCES `local` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_tipo_producto` FOREIGN KEY (`tipo_producto_id`) REFERENCES `tipo_producto` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Filtros para la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `fk_ticket_mesa1` FOREIGN KEY (`mesa_id`) REFERENCES `mesa` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
