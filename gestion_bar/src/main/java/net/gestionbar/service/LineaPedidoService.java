package net.gestionbar.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.gestionbar.dao.implementation.specific.LineaPedidoDao;
import net.gestionbar.dao.implementation.specific.MesaDao;
import net.gestionbar.dao.implementation.specific.ProductoDao;
import net.gestionbar.dao.implementation.specific.TicketDao;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LineaPedidoEntity;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TicketEntity;

@Service
public class LineaPedidoService {
	@Autowired
	LineaPedidoDao oLineaPedidoDao;

	@Autowired
	TicketDao oTicketDao;
	@Autowired
	MesaDao oMesaDao;
	@Autowired
	ProductoDao oProductoDao;
	
	public LineaPedidoEntity get(Long id) {
		return (LineaPedidoEntity) oLineaPedidoDao.get(id);
	}

	public List<LineaPedidoEntity> getall() {
		return oLineaPedidoDao.getall();
	}

	public Long count() {
		return oLineaPedidoDao.count();
	}

	public Page<LineaPedidoEntity> getPage(Pageable oPageable) {
		return oLineaPedidoDao.getPage(oPageable);
	}
	
	public List<LineaPedidoEntity> getFilterId(Long id) {
		TicketEntity oTicketEntity = (TicketEntity) oTicketDao.get(id);
		return  oLineaPedidoDao.getFilter(oTicketEntity);
	}	
	public List<LineaPedidoEntity> getLineaPedido(Long id) {
		MesaEntity oMesaEntity = (MesaEntity) oMesaDao.get(id);
		return  oLineaPedidoDao.getLineaPedido(oMesaEntity);
	}	
	
	public Boolean delete(Long id) {
		return oLineaPedidoDao.delete(id);
	}

	public LineaPedidoEntity create(Map<String, String> mParametros) {
		LineaPedidoEntity oLineaPedidoEntity = new LineaPedidoEntity();
		oLineaPedidoEntity.setCantidad(Long.parseLong(mParametros.get("cantidad")));
		oLineaPedidoEntity.setPrecio(Double.parseDouble(mParametros.get("precio")));
		oLineaPedidoEntity.setProducto((ProductoEntity) oProductoDao.get(Long.parseLong(mParametros.get("producto_id"))));

		oLineaPedidoEntity.setTicket((TicketEntity) oTicketDao.get(Long.parseLong(mParametros.get("ticket_id"))));

		return (LineaPedidoEntity) oLineaPedidoDao.create(oLineaPedidoEntity);
		
	}
	
	public LineaPedidoEntity update(Map<String, String> mParametros)  {
		LineaPedidoEntity oLineaPedidoEntity = new LineaPedidoEntity();
		oLineaPedidoEntity.setId(Long.parseLong(mParametros.get("id")));
		oLineaPedidoEntity.setCantidad(Long.parseLong(mParametros.get("cantidad")));
		oLineaPedidoEntity.setPrecio(Double.parseDouble(mParametros.get("precio")));
		oLineaPedidoEntity.setProducto((ProductoEntity) oProductoDao.get(Long.parseLong(mParametros.get("producto_id"))));

		oLineaPedidoEntity.setTicket((TicketEntity) oTicketDao.get(Long.parseLong(mParametros.get("ticket_id"))));
		return (LineaPedidoEntity) oLineaPedidoDao.update(oLineaPedidoEntity);
	}
	
	public Boolean datosFake(int numLineaPedido) {
		// TODO Auto-generated method stub
		return null;
	}	
	
}
