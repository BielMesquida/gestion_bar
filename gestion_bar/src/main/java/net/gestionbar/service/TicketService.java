package net.gestionbar.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.gestionbar.dao.implementation.specific.LocalDao;
import net.gestionbar.dao.implementation.specific.MesaDao;
import net.gestionbar.dao.implementation.specific.TicketDao;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TicketEntity;
import net.gestionbar.entity.TipoProductoEntity;

@Service
public class TicketService {
	@Autowired
	TicketDao oTicketDao;
	
	@Autowired
	MesaDao oMesaDao;
	
	@Autowired
	LocalDao oLocalDao;
	
	public TicketEntity get(Long id) {
		return (TicketEntity) oTicketDao.get(id);
	}

	public List<TicketEntity> getall() {
		return oTicketDao.getall();
	}

	public Long count() {
		return oTicketDao.count();
	}

	public Page<TicketEntity> getPage(Pageable oPageable) {
		return oTicketDao.getPage(oPageable);
	}

	public Boolean delete(Long id) {
		return oTicketDao.delete(id);
	}
	
	public TicketEntity  create(Map<String, String> mParametros) {
		TicketEntity oTicketEntity = new TicketEntity();
		LocalDateTime datetime = LocalDateTime.parse(mParametros.get("fecha"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		
		oTicketEntity.setFecha(datetime);
		if(mParametros.get("total")!="" && mParametros.get("total")!=null) {
			oTicketEntity.setTotal(Double.parseDouble(mParametros.get("total")));
        }
		oTicketEntity.setPagado(Boolean.parseBoolean(mParametros.get("estado")));
		oTicketEntity.setMesa((MesaEntity) oMesaDao.get(Long.parseLong((mParametros.get("mesa")))));
		return (TicketEntity) oTicketDao.create(oTicketEntity);
	}
	
	public TicketEntity update(Map<String, String> mParametros) {
		LocalDateTime datetime = LocalDateTime.parse(mParametros.get("fecha"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		TicketEntity oTicketEntity = new TicketEntity();
		oTicketEntity.setId(Long.parseLong(mParametros.get("id")));
		oTicketEntity.setFecha(datetime);
		oTicketEntity.setTotal(Double.parseDouble(mParametros.get("total")));
		oTicketEntity.setPagado(Boolean.parseBoolean(mParametros.get("estado")));
		oTicketEntity.setMesa((MesaEntity) oMesaDao.get(Long.parseLong((mParametros.get("mesa")))));
		return (TicketEntity) oTicketDao.update(oTicketEntity);
	}

	public List<TicketEntity> getFilterId(Long id) {
		LocalEntity oLocalEntity = (LocalEntity) oLocalDao.get(id);
		return  oTicketDao.getFilter(oLocalEntity);
	}	
	public Page<TicketEntity> getFilterIdPageable(Pageable oPageable,Long id) {
		LocalEntity oLocalEntity = (LocalEntity) oLocalDao.get(id);
		return  (Page<TicketEntity>) oTicketDao.getFilterPageable(oPageable, oLocalEntity);
	}	
	
	
	public Boolean datosFake(int numTicket) {
		// TODO Auto-generated method stub
		return null;
	}	
	
}
