package net.gestionbar.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.gestionbar.dao.implementation.specific.LocalDao;
import net.gestionbar.dao.implementation.specific.MesaDao;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;

@Service
public class MesaService {
	@Autowired
	MesaDao oMesaDao;
	@Autowired
	LocalDao oLocalDao;
	
	public MesaEntity get(Long id) {
		return (MesaEntity) oMesaDao.get(id);
	}

	public List<MesaEntity> getall() {
		return oMesaDao.getall();
	}
	public List<MesaEntity> getFilterId(Long id){
		LocalEntity oLocalEntity = (LocalEntity) oLocalDao.get(id);
		return  oMesaDao.getFilter(oLocalEntity);
	}
	public Long count() {
		return oMesaDao.count();
	}

	public Page<MesaEntity> getPage(Pageable oPageable) {
		return oMesaDao.getPage(oPageable);
	}

	public Boolean delete(Long id) {
		return oMesaDao.delete(id);
	}

	public MesaEntity create(Map<String, Long> mParametros) {
		MesaEntity oMesaEntity = new MesaEntity();
		oMesaEntity.setNumero(mParametros.get("numero"));
		oMesaEntity.setLocal((LocalEntity) oLocalDao.get(mParametros.get("local_id")));
		return (MesaEntity) oMesaDao.create(oMesaEntity);
	}
	
	public MesaEntity update(Map<String, Long> mParametros) {
		MesaEntity oMesaEntity = new MesaEntity();
		oMesaEntity.setId(mParametros.get("id"));
		oMesaEntity.setNumero(mParametros.get("numero"));
		oMesaEntity.setLocal((LocalEntity) oLocalDao.get(mParametros.get("local_id")));
		return (MesaEntity) oMesaDao.update(oMesaEntity);
	}

	public Boolean datosFake(int numMesa) {
		// TODO Auto-generated method stub
		return null;
	}	
	
}
