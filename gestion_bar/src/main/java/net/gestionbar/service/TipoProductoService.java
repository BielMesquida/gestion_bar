package net.gestionbar.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.gestionbar.dao.implementation.specific.LocalDao;
import net.gestionbar.dao.implementation.specific.TipoProductoDao;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TipoProductoEntity;

@Service
public class TipoProductoService {
	@Autowired
	TipoProductoDao oTipoProductoDao;

	@Autowired
	LocalDao oLocalDao;
	
	public TipoProductoEntity get(Long id) {
		return (TipoProductoEntity) oTipoProductoDao.get(id);
	}

	public List<TipoProductoEntity> getall() {
		return oTipoProductoDao.getall();
	}

	public Long count() {
		return oTipoProductoDao.count();
	}

	public Page<TipoProductoEntity> getPage(Pageable oPageable) {
		return oTipoProductoDao.getPage(oPageable);
	}
	
	public Page<TipoProductoEntity> getFilterId(Pageable oPageable, Long id) {
		LocalEntity oLocalEntity = (LocalEntity) oLocalDao.get(id);
		return  (Page<TipoProductoEntity>)oTipoProductoDao.getFilter(oPageable,oLocalEntity);
	}	

	public List<TipoProductoEntity> getAllFilterId(Long id) {
		LocalEntity oLocalEntity = (LocalEntity) oLocalDao.get(id);
		return  (List<TipoProductoEntity>)oTipoProductoDao.getAllFilter(oLocalEntity);
	}	
	public Boolean delete(Long id) {
		return oTipoProductoDao.delete(id);
	}

	public TipoProductoEntity create(Map<String, String> mParametros)  {
		TipoProductoEntity oTipoProductoEntity = new TipoProductoEntity();
		oTipoProductoEntity.setDescripcion(mParametros.get("descripcion"));
		oTipoProductoEntity.setLocal((LocalEntity) oLocalDao.get(Long.parseLong((mParametros.get("local")))));	
		return (TipoProductoEntity) oTipoProductoDao.create(oTipoProductoEntity);
	}
	
	public TipoProductoEntity update(Map<String, String> mParametros) {
		TipoProductoEntity oTipoProductoEntity = new TipoProductoEntity();
		oTipoProductoEntity.setId(Long.parseLong(mParametros.get("id")));
		oTipoProductoEntity.setDescripcion(mParametros.get("descripcion"));
		oTipoProductoEntity.setLocal((LocalEntity) oLocalDao.get(Long.parseLong((mParametros.get("local")))));	
		return (TipoProductoEntity) oTipoProductoDao.update(oTipoProductoEntity);
	}

}
