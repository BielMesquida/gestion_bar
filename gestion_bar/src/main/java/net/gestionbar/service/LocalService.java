package net.gestionbar.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.gestionbar.dao.implementation.specific.LocalDao;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LocalEntity;

@Service
public class LocalService {
	@Autowired
	LocalDao oLocalDao;

	public LocalEntity get(Long id) {
		return (LocalEntity) oLocalDao.get(id);
	}

	public List<LocalEntity> getall() {
		return oLocalDao.getall();
	}

	public Long count() {
		return oLocalDao.count();
	}

	public Page<LocalEntity> getPage(Pageable oPageable) {
		return oLocalDao.getPage(oPageable);
	}

	public Boolean delete(Long id) {
		return oLocalDao.delete(id);
	}

	public LocalEntity create(GenericEntityInterface oLocalEntity) {
		return (LocalEntity) oLocalDao.create(oLocalEntity);
	}

	public LocalEntity update(GenericEntityInterface oLocalEntity) {
		return (LocalEntity) oLocalDao.update(oLocalEntity);
	}

	public LocalEntity login(Map<String, String> mParametros) {
		LocalEntity oLocalEntity = new LocalEntity();
		oLocalEntity.setLogin(mParametros.get("login"));
		oLocalEntity.setPassword(mParametros.get("password"));
		return (LocalEntity) oLocalDao.login(oLocalEntity);
	}

	public Boolean datosFake(int numLocal) {
		String[] frasesInicio = { "Bar ", "Restaurante  ","Pastisseria  " };
		String[] frasesFinal = { "Paco", "Paquita", "Ramon", "Toni","Maria" };
		
		for (int i = 0; i < numLocal; i++) {
			LocalEntity oLocalEntityFake = new LocalEntity();
			String fraseRandom = "";
			fraseRandom += frasesInicio[(int) (Math.random() * frasesInicio.length) + 0];
			fraseRandom += frasesFinal[(int) (Math.random() * frasesFinal.length) + 0];
			String login = frasesFinal[(int) (Math.random() * frasesFinal.length) + 0] + (int) Math.floor(Math.random() * (1000 - 9999) + 9999);
		
			oLocalEntityFake.setDescripcion(fraseRandom);
			oLocalEntityFake.setNif((int) Math.floor(Math.random() * (10000000 - 99999999) + 99999999) + "O");	
			oLocalEntityFake.setEmail(login + "@cloudtpv.com");
			oLocalEntityFake.setTelefono((long) Math.floor(Math.random() * (100000000 - 999999999) + 999999999) );	
			oLocalEntityFake.setLogin(login);
			oLocalEntityFake.setPassword("da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04");

			oLocalDao.create(oLocalEntityFake);
		}
		return true;

	}

}
