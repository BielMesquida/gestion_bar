package net.gestionbar.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.gestionbar.dao.implementation.specific.LocalDao;
import net.gestionbar.dao.implementation.specific.ProductoDao;
import net.gestionbar.dao.implementation.specific.TipoProductoDao;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TipoProductoEntity;
import net.gestionbar.dao.implementation.specific.TipoProductoDao;


@Service
public class ProductoService {
	@Autowired
	ProductoDao oProductoDao;

	@Autowired
	TipoProductoDao oTipoProductoDao;
	
	@Autowired
	LocalDao oLocalDao;
	
	public ProductoEntity get(Long id) {
		return (ProductoEntity) oProductoDao.get(id);
	}

	public List<ProductoEntity> getall() {
		return oProductoDao.getall();
	}

	public Long count() {
		return oProductoDao.count();
	}

	public Page<ProductoEntity> getPage(Pageable oPageable) {
		return oProductoDao.getPage(oPageable);
	}

	public Boolean delete(Long id) {
		return oProductoDao.delete(id);
	}

	public ProductoEntity create(Map<String, String> mParametros) {
		ProductoEntity oProductoEntity = new ProductoEntity();
		oProductoEntity.setDescripcion(mParametros.get("descripcion"));
		oProductoEntity.setCodigo(mParametros.get("codigo"));
		oProductoEntity.setPrecio(Double.parseDouble(mParametros.get("precio")));
		oProductoEntity.setImagen(mParametros.get("imagen"));
		oProductoEntity.setTipo_producto((TipoProductoEntity) oTipoProductoDao.get(Long.parseLong((mParametros.get("tipo_producto")))));
		oProductoEntity.setLocal((LocalEntity) oLocalDao.get(Long.parseLong((mParametros.get("local")))));
		return (ProductoEntity) oProductoDao.create(oProductoEntity);
	}
	
	public ProductoEntity update(Map<String, String> mParametros) {
		ProductoEntity oProductoEntity = new ProductoEntity();
		oProductoEntity.setId(Long.parseLong(mParametros.get("id")));
		oProductoEntity.setDescripcion(mParametros.get("descripcion"));
		oProductoEntity.setCodigo(mParametros.get("codigo"));
		oProductoEntity.setPrecio(Double.parseDouble(mParametros.get("precio")));
		oProductoEntity.setImagen(mParametros.get("imagen"));
		oProductoEntity.setTipo_producto((TipoProductoEntity) oTipoProductoDao.get(Long.parseLong((mParametros.get("tipo_producto")))));
		oProductoEntity.setLocal((LocalEntity) oLocalDao.get(Long.parseLong((mParametros.get("local")))));	
		return (ProductoEntity) oProductoDao.update(oProductoEntity);
	}

	public Page<ProductoEntity> getFilterId(Pageable oPageable,Long id) {
		LocalEntity oLocalEntity = (LocalEntity) oLocalDao.get(id);
		return  (Page<ProductoEntity>) oProductoDao.getFilter(oPageable, oLocalEntity);
	}	
	

}
