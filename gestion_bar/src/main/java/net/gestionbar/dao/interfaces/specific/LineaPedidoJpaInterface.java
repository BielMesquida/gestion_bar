package net.gestionbar.dao.interfaces.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import net.gestionbar.entity.LineaPedidoEntity;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TicketEntity;
import net.gestionbar.entity.TipoProductoEntity;



public interface LineaPedidoJpaInterface extends JpaRepository<LineaPedidoEntity, Long> {

	@Query(value="SELECT m FROM LineaPedidoEntity m WHERE m.ticket = :ticket_id ")
	
	public List<LineaPedidoEntity> getFilter(@Param("ticket_id") TicketEntity oTicketEntity) ;
	
	@Query(value="SELECT lp FROM LineaPedidoEntity lp, TicketEntity t WHERE t.id = lp.ticket AND t.pagado = 0 AND t.mesa = :mesa_id " )
	
	public List<LineaPedidoEntity> getLineaPedido(@Param("mesa_id") MesaEntity oMesaEntity) ;



	
}
