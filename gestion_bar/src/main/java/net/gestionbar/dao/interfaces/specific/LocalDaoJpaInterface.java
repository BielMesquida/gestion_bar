package net.gestionbar.dao.interfaces.specific;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.TipoProductoEntity;



public interface LocalDaoJpaInterface extends JpaRepository<LocalEntity, Long> {
	@Query(value="SELECT l FROM LocalEntity l WHERE l.login = :login AND l.password = :password")
	
	public LocalEntity findByLogin(@Param("login") String login, @Param("password") String password) ;
		 
		
	
	
}
