package net.gestionbar.dao.interfaces.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TicketEntity;
import net.gestionbar.entity.TipoProductoEntity;



public interface TicketDaoJpaInterface extends JpaRepository<TicketEntity, Long> {
	@Query(value="SELECT t FROM TicketEntity t, MesaEntity m WHERE t.mesa = m AND m.local = :local_id ")

	public List<TicketEntity> getFilter(@Param("local_id") LocalEntity oLocalEntity);

	@Query(value="SELECT t FROM TicketEntity t, MesaEntity m WHERE t.mesa = m AND m.local = :local_id ")

	public Page<TicketEntity> getFilterPageable(org.springframework.data.domain.Pageable oPageable,@Param("local_id") LocalEntity oLocalEntity);

}
