package net.gestionbar.dao.interfaces.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TipoProductoEntity;



public interface MesaDaoJpaInterface extends JpaRepository<MesaEntity, Long> {
	@Query(value="SELECT m FROM MesaEntity m WHERE m.local = :local_id ")
	
	public List<MesaEntity> getFilter(@Param("local_id") LocalEntity oLocalEntity) ;
	
	
}
