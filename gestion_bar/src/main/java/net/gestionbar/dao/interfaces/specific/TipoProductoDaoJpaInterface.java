package net.gestionbar.dao.interfaces.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TipoProductoEntity;



public interface TipoProductoDaoJpaInterface extends JpaRepository<TipoProductoEntity, Long> {
	@Query(value="SELECT p FROM TipoProductoEntity p WHERE p.local = :local_id ")
	public Page<TipoProductoEntity> getFilter(org.springframework.data.domain.Pageable oPageable,@Param("local_id") LocalEntity oLocalEntity);

	@Query(value="SELECT p FROM TipoProductoEntity p WHERE p.local = :local_id ")
	public List<TipoProductoEntity> getAllFilter(@Param("local_id") LocalEntity oLocalEntity);
}

