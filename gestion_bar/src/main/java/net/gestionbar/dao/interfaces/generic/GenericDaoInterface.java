package net.gestionbar.dao.interfaces.generic;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.gestionbar.entity.GenericEntityInterface;

public interface GenericDaoInterface<T extends GenericEntityInterface> {

	T get(Long id);

	List<T> getall();

	long count();

	Page<T> getPage(Pageable oPageable);

	Boolean delete(Long id);

	T create(T oEntity);

	T update(T oEntity);

}