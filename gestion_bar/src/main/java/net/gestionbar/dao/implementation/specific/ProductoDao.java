package net.gestionbar.dao.implementation.specific;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import net.gestionbar.dao.implementation.generic.GenericDaoImplementation;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.dao.interfaces.specific.ProductoDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.TipoProductoDaoJpaInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.ProductoEntity;

@Repository
public class ProductoDao extends GenericDaoImplementation implements GenericDaoInterface {
	@Autowired
	protected ProductoDaoJpaInterface oJpaRepository;


	@Autowired
	public ProductoDao(ProductoDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}


	public Page<ProductoEntity> getFilter(Pageable oPageable, LocalEntity oLocalEntity) {
		Page<ProductoEntity> oProductoEntity = oJpaRepository.getFilter(oPageable, oLocalEntity);
		  return  oProductoEntity;

}

}
