package net.gestionbar.dao.implementation.specific;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import net.gestionbar.dao.implementation.generic.GenericDaoImplementation;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.dao.interfaces.specific.ProductoDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.TicketDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.TipoProductoDaoJpaInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TicketEntity;

@Repository
public class TicketDao extends GenericDaoImplementation implements GenericDaoInterface {
	@Autowired
	protected TicketDaoJpaInterface oJpaRepository;


	@Autowired
	public TicketDao(TicketDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	public List<TicketEntity> getFilter(LocalEntity oLocalEntity) {
		 List<TicketEntity> oTicketEntity = oJpaRepository.getFilter(oLocalEntity);
		  return  oTicketEntity;


	}
	public Page<TicketEntity> getFilterPageable(Pageable oPageable, LocalEntity oLocalEntity) {
		Page<TicketEntity> oTicketEntity = oJpaRepository.getFilterPageable(oPageable, oLocalEntity);
		  return  oTicketEntity;


	}
	
}
