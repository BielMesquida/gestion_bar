package net.gestionbar.dao.implementation.specific;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import net.gestionbar.dao.implementation.generic.GenericDaoImplementation;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.dao.interfaces.specific.TipoProductoDaoJpaInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TipoProductoEntity;

@Repository
public class TipoProductoDao extends GenericDaoImplementation implements GenericDaoInterface {
	@Autowired
	protected TipoProductoDaoJpaInterface oJpaRepository;


	@Autowired
	public TipoProductoDao(TipoProductoDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	public Page<TipoProductoEntity> getFilter(Pageable oPageable, LocalEntity oLocalEntity) {
		 Page<TipoProductoEntity> oTipoProductoEntity = oJpaRepository.getFilter(oPageable, oLocalEntity);
		  return  oTipoProductoEntity;

}

	public List<TipoProductoEntity> getAllFilter(LocalEntity oLocalEntity) {
		List<TipoProductoEntity> oTipoProductoEntity = oJpaRepository.getAllFilter(oLocalEntity);
		return oTipoProductoEntity;
	}
	
}
