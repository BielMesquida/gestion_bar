package net.gestionbar.dao.implementation.specific;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import net.gestionbar.dao.implementation.generic.GenericDaoImplementation;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.dao.interfaces.specific.LineaPedidoJpaInterface;
import net.gestionbar.dao.interfaces.specific.ProductoDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.TipoProductoDaoJpaInterface;
import net.gestionbar.entity.LineaPedidoEntity;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.TicketEntity;

@Repository
public class LineaPedidoDao extends GenericDaoImplementation implements GenericDaoInterface {
	@Autowired
	protected LineaPedidoJpaInterface oJpaRepository;


	@Autowired
	public LineaPedidoDao(LineaPedidoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	  public List<LineaPedidoEntity> getFilter(TicketEntity oTicketEntity) {
			 List<LineaPedidoEntity> oLineaPedidoEntity = oJpaRepository.getFilter(oTicketEntity);
					  return  oLineaPedidoEntity;
			
		}
	  public List<LineaPedidoEntity> getLineaPedido(MesaEntity oMesaEntity){
		  List<LineaPedidoEntity> oLineaPedidoEntity = oJpaRepository.getLineaPedido(oMesaEntity);
		  return oLineaPedidoEntity;
	  }
}
