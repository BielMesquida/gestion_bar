package net.gestionbar.dao.implementation.specific;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import net.gestionbar.dao.implementation.generic.GenericDaoImplementation;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.dao.interfaces.specific.MesaDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.ProductoDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.TipoProductoDaoJpaInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.entity.MesaEntity;

@Repository
public class MesaDao extends GenericDaoImplementation implements GenericDaoInterface {
	@Autowired
	protected MesaDaoJpaInterface oJpaRepository;


	@Autowired
	public MesaDao(MesaDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	  public List<MesaEntity> getFilter(LocalEntity oLocalEntity) {
		 List<MesaEntity> oMesaEntity = oJpaRepository.getFilter(oLocalEntity);
				  return  oMesaEntity;
		
	}
}
