package net.gestionbar.dao.implementation.specific;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import net.gestionbar.dao.implementation.generic.GenericDaoImplementation;
import net.gestionbar.dao.interfaces.generic.GenericDaoInterface;
import net.gestionbar.dao.interfaces.specific.LocalDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.ProductoDaoJpaInterface;
import net.gestionbar.dao.interfaces.specific.TipoProductoDaoJpaInterface;
import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LocalEntity;

@Repository
public class LocalDao extends GenericDaoImplementation implements GenericDaoInterface {
	@Autowired
	protected LocalDaoJpaInterface oJpaRepository;


	@Autowired
	public LocalDao(LocalDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}



  public LocalEntity login(LocalEntity oLocalEntity) {
	  LocalEntity oLocalEntity2 = oJpaRepository.findByLogin(oLocalEntity.getLogin(),oLocalEntity.getPassword());
	  return oLocalEntity2;
	
}
	
 
}
