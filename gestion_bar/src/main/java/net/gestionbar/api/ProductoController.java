package net.gestionbar.api;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.service.ProductoService;

import org.springframework.web.bind.annotation.PutMapping;

@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/producto")
public class ProductoController {

	@Autowired
	ProductoService oProductoService;

	@GetMapping("/{id}")
	public ResponseEntity<ProductoEntity> get(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<ProductoEntity>(oProductoService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<ProductoEntity>> get() {
		return new ResponseEntity<List<ProductoEntity>>(oProductoService.getall(), HttpStatus.OK);
	}
	
	@GetMapping("/filter/{page}/{rpp}/{local_id}")
	public ResponseEntity<Page<ProductoEntity>> getFilter(@PathVariable(value="page") int page, @PathVariable(value = "rpp") int rpp, 
														  @PathVariable(value = "local_id") Long local_id) {
		Pageable oPageable;
		oPageable = PageRequest.of(page,rpp);
		return new ResponseEntity<Page<ProductoEntity>>( oProductoService.getFilterId(oPageable, local_id), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oProductoService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<Page<ProductoEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<Page<ProductoEntity>>(oProductoService.getPage(oPageable), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<Boolean>(oProductoService.delete(id), HttpStatus.OK);
	}

	@PostMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<ProductoEntity> create(@RequestBody  Map<String, String> mParametros/*GenericEntityInterface oTipousuarioBean*/) {
		return new ResponseEntity<ProductoEntity>(oProductoService.create(mParametros), HttpStatus.OK);
	}

	@PutMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<ProductoEntity> update(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<ProductoEntity>(oProductoService.update(mParametros), HttpStatus.OK);
	}

	
}
