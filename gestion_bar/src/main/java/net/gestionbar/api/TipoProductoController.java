
package net.gestionbar.api;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TipoProductoEntity;
import net.gestionbar.service.TipoProductoService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/tipoproducto")
public class TipoProductoController {

	@Autowired
	TipoProductoService oTipoProductoService;	

	@GetMapping("/{id}")
	public ResponseEntity<TipoProductoEntity> get(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<TipoProductoEntity>(oTipoProductoService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<TipoProductoEntity>> get() {
		return new ResponseEntity<List<TipoProductoEntity>>(oTipoProductoService.getall(), HttpStatus.OK);
	}
	
	@GetMapping("/filter/{page}/{rpp}/{local_id}")
	public ResponseEntity<Page<TipoProductoEntity>> getFilter(@PathVariable(value="page") int page, @PathVariable(value = "rpp") int rpp, 
			  @PathVariable(value = "local_id") Long local_id) {
		Pageable oPageable;
		oPageable = PageRequest.of(page,rpp);
		return new ResponseEntity<Page<TipoProductoEntity>>( oTipoProductoService.getFilterId(oPageable,local_id), HttpStatus.OK);
	}
	
	@GetMapping("/filter/{local_id}")
	public ResponseEntity<List<TipoProductoEntity>> getAllFilter(@PathVariable(value = "local_id") Long local_id) {
		return new ResponseEntity<List<TipoProductoEntity>>( oTipoProductoService.getAllFilterId(local_id), HttpStatus.OK);
	}
	
	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oTipoProductoService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<Page<TipoProductoEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<Page<TipoProductoEntity>>(oTipoProductoService.getPage(oPageable), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<Boolean>(oTipoProductoService.delete(id), HttpStatus.OK);
	}

	@PostMapping("/") //@RequestParam para uso parametro a parametro
	public ResponseEntity<TipoProductoEntity> create(@RequestBody  Map<String, String> mParametros) {
		return new ResponseEntity<TipoProductoEntity>(oTipoProductoService.create( mParametros), HttpStatus.OK);
	}
	@PutMapping("/") //@RequestParam para uso parametro a parametro
	public ResponseEntity<TipoProductoEntity> update(@RequestBody  Map<String, String> mParametros) {
		return new ResponseEntity<TipoProductoEntity>(oTipoProductoService.update( mParametros), HttpStatus.OK);
	}	
	
}
