package net.gestionbar.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.service.LocalService;

import org.springframework.web.bind.annotation.PutMapping;

@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/local")
public class LocalController {

	@Autowired
	LocalService oLocalService;

	@GetMapping("/{id}")
	public ResponseEntity<LocalEntity> get(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<LocalEntity>(oLocalService.get(id), HttpStatus.OK);
	}
	@GetMapping("/fill/{numLocal}")
	public ResponseEntity<Boolean> datosFake(@PathVariable(value = "numLocal") int numLocal) {
		return new ResponseEntity<Boolean>(oLocalService.datosFake(numLocal),HttpStatus.OK);
	}
	@GetMapping("/getall")
	public ResponseEntity<List<LocalEntity>> get() {
		return new ResponseEntity<List<LocalEntity>>(oLocalService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oLocalService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<Page<LocalEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<Page<LocalEntity>>(oLocalService.getPage(oPageable), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<Boolean>(oLocalService.delete(id), HttpStatus.OK);
	}

	@PostMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<LocalEntity> create(@RequestBody LocalEntity oLocalBean) {
		return new ResponseEntity<LocalEntity>(oLocalService.create(oLocalBean), HttpStatus.OK);
	}

	@PutMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<LocalEntity> update(@RequestBody LocalEntity oLocalBean) {
		return new ResponseEntity<LocalEntity>(oLocalService.update(oLocalBean), HttpStatus.OK);
	}

}
