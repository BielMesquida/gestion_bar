package net.gestionbar.api;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.ProductoEntity;
import net.gestionbar.entity.TicketEntity;
import net.gestionbar.service.TicketService;

import org.springframework.web.bind.annotation.PutMapping;

@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/ticket")
public class TicketController {

	@Autowired
	TicketService oTicketService;

	@GetMapping("/{id}")
	public ResponseEntity<TicketEntity> get(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<TicketEntity>(oTicketService.get(id), HttpStatus.OK);
	}
	@GetMapping("/fill/{numTicket}")
	public ResponseEntity<Boolean> datosFake(@PathVariable(value = "numTicket") int numTicket) {
		return new ResponseEntity<Boolean>(oTicketService.datosFake(numTicket),HttpStatus.OK);
	}

	@GetMapping("/filter/{page}/{rpp}/{local_id}")
	public ResponseEntity<Page<TicketEntity>> getFilterPageable(@PathVariable(value="page") int page, @PathVariable(value = "rpp") int rpp, 
			  @PathVariable(value = "local_id") Long local_id) {
		Pageable oPageable;
		oPageable = PageRequest.of(page,rpp);
		return new ResponseEntity<Page<TicketEntity>>( oTicketService.getFilterIdPageable(oPageable,local_id), HttpStatus.OK);
	}
	
	@GetMapping("/filter/{local_id}")
	public ResponseEntity<List<TicketEntity>> getFilter(@PathVariable(value = "local_id") Long local_id) {
		System.out.println(local_id);

		return new ResponseEntity<List<TicketEntity>>( oTicketService.getFilterId(local_id), HttpStatus.OK);
	}
	
	@GetMapping("/getall")
	public ResponseEntity<List<TicketEntity>> get() {
		return new ResponseEntity<List<TicketEntity>>(oTicketService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oTicketService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<Page<TicketEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<Page<TicketEntity>>(oTicketService.getPage(oPageable), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<Boolean>(oTicketService.delete(id), HttpStatus.OK);
	}

	@PostMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<TicketEntity> create(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<TicketEntity>(oTicketService.create(mParametros), HttpStatus.OK);
	}

	@PutMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<TicketEntity> update(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<TicketEntity>(oTicketService.update(mParametros), HttpStatus.OK);
	}

}
