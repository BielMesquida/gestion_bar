package net.gestionbar.api;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.MesaEntity;
import net.gestionbar.service.MesaService;

import org.springframework.web.bind.annotation.PutMapping;

@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/mesa")
public class MesaController {

	@Autowired
	MesaService oMesaService;

	@GetMapping("/{id}")
	public ResponseEntity<MesaEntity> get(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<MesaEntity>(oMesaService.get(id), HttpStatus.OK);
	}
	@GetMapping("/filter/{local_id}")
	public ResponseEntity<List<MesaEntity>> getFilter(@PathVariable(value = "local_id") Long local_id) {
		return new ResponseEntity<List<MesaEntity>>( oMesaService.getFilterId(local_id), HttpStatus.OK);
	}
	@GetMapping("/fill/{numMesa}")
	public ResponseEntity<Boolean> datosFake(@PathVariable(value = "numMesa") int numMesa) {
		return new ResponseEntity<Boolean>(oMesaService.datosFake(numMesa),HttpStatus.OK);
	}
	@GetMapping("/getall")
	public ResponseEntity<List<MesaEntity>> get() {
		return new ResponseEntity<List<MesaEntity>>(oMesaService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oMesaService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<Page<MesaEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<Page<MesaEntity>>(oMesaService.getPage(oPageable), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<Boolean>(oMesaService.delete(id), HttpStatus.OK);
	}

	@PostMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<MesaEntity> create(@RequestBody Map<String, Long> mParametros/*@RequestBody MesaEntity oMesaBean*/) {
		//System.out.println(mParametros.toString());
		return new ResponseEntity<MesaEntity>(oMesaService.create(mParametros), HttpStatus.OK);
	}

	@PutMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<MesaEntity> update(@RequestBody Map<String, Long> mParametros/*@RequestBody MesaEntity oMesaBean*/) {
		return new ResponseEntity<MesaEntity>(oMesaService.update(mParametros), HttpStatus.OK);
	}

}
