package net.gestionbar.api;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.LineaPedidoEntity;
import net.gestionbar.entity.TipoProductoEntity;
import net.gestionbar.service.LineaPedidoService;

import org.springframework.web.bind.annotation.PutMapping;

@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/linea_pedido")
public class LineaPedidoController {

	@Autowired
	LineaPedidoService oLineaPedidoService;

	@GetMapping("/{id}")
	public ResponseEntity<LineaPedidoEntity> get(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<LineaPedidoEntity>(oLineaPedidoService.get(id), HttpStatus.OK);
	}
	@GetMapping("/fill/{numLineaPedido}")
	public ResponseEntity<Boolean> datosFake(@PathVariable(value = "numLineaPedido") int numLineaPedido) {
		return new ResponseEntity<Boolean>(oLineaPedidoService.datosFake(numLineaPedido),HttpStatus.OK);
	}
	@GetMapping("/filter/{ticket_id}")
	public ResponseEntity<List<LineaPedidoEntity>> getFilter(@PathVariable(value = "ticket_id") Long ticket_id) {

		return new ResponseEntity<List<LineaPedidoEntity>>( oLineaPedidoService.getFilterId(ticket_id), HttpStatus.OK);
	}
	
	@GetMapping("/lp/{mesa_id}")
	public ResponseEntity<List<LineaPedidoEntity>> getLineaPedido(@PathVariable(value = "mesa_id") Long ticket_id) {
		
		return new ResponseEntity<List<LineaPedidoEntity>>( oLineaPedidoService.getLineaPedido(ticket_id), HttpStatus.OK);
	}	
	@GetMapping("/getall")
	public ResponseEntity<List<LineaPedidoEntity>> get() {
		return new ResponseEntity<List<LineaPedidoEntity>>(oLineaPedidoService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oLineaPedidoService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<Page<LineaPedidoEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<Page<LineaPedidoEntity>>(oLineaPedidoService.getPage(oPageable), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<Boolean>(oLineaPedidoService.delete(id), HttpStatus.OK);
	}

	@PostMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<LineaPedidoEntity> create(@RequestBody  Map<String, String> mParametros) {
		System.out.println(mParametros);
		return new ResponseEntity<LineaPedidoEntity>(oLineaPedidoService.create(mParametros), HttpStatus.OK);
	}

	@PutMapping("/") // @RequestParam para uso parametro a parametro
	public ResponseEntity<LineaPedidoEntity> update(@RequestBody  Map<String, String> mParametros) {
		return new ResponseEntity<LineaPedidoEntity>(oLineaPedidoService.update(mParametros), HttpStatus.OK);
	}

}
