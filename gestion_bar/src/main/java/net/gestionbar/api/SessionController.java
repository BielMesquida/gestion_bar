package net.gestionbar.api;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.gestionbar.entity.GenericEntityInterface;
import net.gestionbar.entity.TipoProductoEntity;
import net.gestionbar.entity.LocalEntity;
import net.gestionbar.service.LocalService;

import org.springframework.util.MultiValueMap;

@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/session")
public class SessionController {

	@Autowired
	HttpSession oSession;

	@Autowired
	LocalService oLocalService;

	@GetMapping("/") // check
	public ResponseEntity<LocalEntity> check() {
		LocalEntity oLocalEntity = (LocalEntity) oSession.getAttribute("login");
		return new ResponseEntity<LocalEntity>(oLocalEntity, HttpStatus.OK);
	}

	@DeleteMapping("/") // logout
	public ResponseEntity<Boolean> logout() {
		oSession.invalidate();
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	@PostMapping("/") // login
	public ResponseEntity<LocalEntity> login(@RequestParam Map<String, String> mParametros) {
				oSession.setAttribute("login", oLocalService.login(mParametros)) ;

		return new ResponseEntity<LocalEntity>(	(LocalEntity) oSession.getAttribute("login"), HttpStatus.OK);
	//	return new ResponseEntity<LocalEntity>(oLocalService.login(mParametros), HttpStatus.OK);

	}
	
}
