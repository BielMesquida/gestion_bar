package net.gestionbar.entity;

public interface GenericEntityInterface {
    
    public Long getId();

    public void setId(Long id);

}
