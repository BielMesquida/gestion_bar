package net.gestionbar.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tipo_producto")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class TipoProductoEntity implements Serializable, GenericEntityInterface {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id") // si tiene el mismo nombre no hace falta
	private Long id;
	private String descripcion;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tipo_producto")
	private List<ProductoEntity> productos = new ArrayList<>();

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="local_id")
	private LocalEntity local;
	
	public TipoProductoEntity() {
		super();
	}

	public TipoProductoEntity(Long id, String descripcion, LocalEntity local) {
		this.id = id;
		this.descripcion = descripcion;
		this.local = local;
	}

	public TipoProductoEntity(String descripcion, LocalEntity local) {
		this.descripcion = descripcion;
		this.local = local;
	}
	
	

	public LocalEntity getLocal() {
		return local;
	}

	public void setLocal(LocalEntity local) {
		this.local = local;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getProductos() {
		return productos.size();
	}

	public void setProductos(List<ProductoEntity> productos) {
		this.productos = productos;
	}


}
