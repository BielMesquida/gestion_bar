package net.gestionbar.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "ticket")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class TicketEntity implements Serializable, GenericEntityInterface {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	private LocalDateTime fecha;
	private Double total;
	private Boolean pagado;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ticket")
	private List<LineaPedidoEntity> lineas_pedido1 = new ArrayList<>();
		
	@ManyToOne(fetch=FetchType.EAGER )
	@JoinColumn(name="mesa_id")
	private MesaEntity mesa;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public LocalDateTime getFecha() {
		return fecha;
	}


	public void setFecha(LocalDateTime datetime) {
		this.fecha = datetime;
	}


	public Double getTotal() {
		return total;
	}


	public void setTotal(Double total) {
		this.total = total;
	}


	public int getLineas_pedido1() {
		return lineas_pedido1.size();
	}


	public Boolean getPagado() {
		return pagado;
	}


	public void setPagado(Boolean pagado) {
		this.pagado = pagado;
	}


	public void setLineas_pedido(List<LineaPedidoEntity> lineas_pedido1) {
		this.lineas_pedido1 = lineas_pedido1;
	}


	public MesaEntity getMesa() {
		return mesa;
	}


	public void setMesa(MesaEntity mesa) {
		this.mesa = mesa;
	}


	public TicketEntity(LocalDateTime fecha, Double total, List<LineaPedidoEntity> lineas_pedido1, MesaEntity mesa) {
		super();
		this.fecha = fecha;
		this.total = total;
		this.lineas_pedido1 = lineas_pedido1;
		this.mesa = mesa;
	}


	public TicketEntity(Long id, LocalDateTime fecha, Double total, List<LineaPedidoEntity> lineas_pedido1, MesaEntity mesa) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.total = total;
		this.lineas_pedido1 = lineas_pedido1;
		this.mesa = mesa;
	}


	public TicketEntity() {
		super();
	}







	
	
}
