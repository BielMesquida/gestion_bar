package net.gestionbar.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "linea_pedido")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LineaPedidoEntity implements Serializable, GenericEntityInterface {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	private Long cantidad;
	private Double precio;
	
	@ManyToOne(fetch=FetchType.LAZY )
	@JoinColumn(name="producto_id")
	private ProductoEntity producto;
	
	@ManyToOne(fetch=FetchType.LAZY )
	@JoinColumn(name="ticket_id")
	private TicketEntity ticket;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public TicketEntity getTicket() {
		return ticket;
	}

	public void setTicket(TicketEntity ticket) {
		this.ticket = ticket;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public LineaPedidoEntity(Long id, Long cantidad, ProductoEntity producto, Double precio, TicketEntity ticket) {
		super();
		this.id = id;
		this.cantidad = cantidad;
		this.precio = precio;
		this.producto = producto;
		this.ticket = ticket;
	}

	public LineaPedidoEntity(Long cantidad, ProductoEntity producto, Double precio, TicketEntity ticket) {
		super();
		this.cantidad = cantidad;
		this.precio = precio;
		this.producto = producto;
		this.ticket = ticket;
	}

	public LineaPedidoEntity() {
		super();
	}
	
	
		
	
}
