package net.gestionbar.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@Table(name = "local")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LocalEntity implements Serializable, GenericEntityInterface {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	private String descripcion;
	private String nif;
	private String email;
	private Long telefono;
	private String login;
	//@JsonIgnore
	private String password;
		
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "local")
	private List<MesaEntity> mesas = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "local")
	private List<ProductoEntity> productos = new ArrayList<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getTelefono() {
		return telefono;
	}

	public void setTelefono(Long telefono) {
		this.telefono = telefono;
	}

	public LocalEntity(String descripcion, String nif, String email, Long telefono, String login, String password) {
		super();
		this.descripcion = descripcion;
		this.nif = nif;
		this.email = email;
		this.telefono = telefono;
		this.login = login;
		this.password = password;
	}

	public LocalEntity(Long id, String descripcion, String nif, String email, Long telefono, String login,
			String password) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.nif = nif;
		this.email = email;
		this.telefono = telefono;
		this.login = login;
		this.password = password;
	}

	public LocalEntity() {
		super();
	}

	public int getMesas() {
		return mesas.size();
	}
	
	

	public void setMesas(List<MesaEntity> mesas) {
		this.mesas = mesas;
	}
	
	public int getProductos() {
		return productos.size();
	}
	
	public void setProductos(List<ProductoEntity> productos) {
		this.productos = productos;
	}
	@Override
	public String toString() {
		return "LocalEntity [id=" + id + ", descripcion=" + descripcion + ", nif=" + nif + ", email=" + email
				+ ", telefono=" + telefono + ", login=" + login + ", password=" + password + ", mesas=" + mesas + "]";
	}



}
