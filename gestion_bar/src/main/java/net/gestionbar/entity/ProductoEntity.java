package net.gestionbar.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "producto")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProductoEntity implements Serializable, GenericEntityInterface {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	private String codigo;
	private Double precio;
	private String imagen;
	private String descripcion;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "producto")
	private List<LineaPedidoEntity> lineas_pedido = new ArrayList<>();
	
		
	@ManyToOne(fetch=FetchType.EAGER )
	@JoinColumn(name="tipo_producto_id")
	private TipoProductoEntity tipo_producto;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="local_id")
	private LocalEntity local;
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public Double getPrecio() {
		return precio;
	}


	public void setPrecio(Double precio) {
		this.precio = precio;
	}


	public String getImagen() {
		return imagen;
	}


	public void setImagen(String imagen) {
		this.imagen = imagen;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public TipoProductoEntity getTipo_producto() {
		return tipo_producto;
	}


	public void setTipo_producto(TipoProductoEntity tipo_producto) {
		this.tipo_producto = tipo_producto;
	}


	public ProductoEntity() {
	}


	public LocalEntity getLocal() {
		return local;
	}


	public void setLocal(LocalEntity local) {
		this.local = local;
	}


	public ProductoEntity(String codigo, Long existencias, Double precio, String imagen, String descripcion,
			TipoProductoEntity tipo_producto, LocalEntity local) {
		super();
		this.codigo = codigo;
		this.precio = precio;
		this.imagen = imagen;
		this.descripcion = descripcion;
		this.tipo_producto = tipo_producto;
		this.local = local;
	}


	public ProductoEntity(Long id, String codigo, Long existencias, Double precio, String imagen, String descripcion,
			TipoProductoEntity tipo_producto, LocalEntity local) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.precio = precio;
		this.imagen = imagen;
		this.descripcion = descripcion;
		this.tipo_producto = tipo_producto;
		this.local = local;

	}


	public int getLineas_pedido() {
		return lineas_pedido.size();
	}


	public void setLineas_pedido(List<LineaPedidoEntity> lineas_pedido) {
		this.lineas_pedido = lineas_pedido;
	}


}
