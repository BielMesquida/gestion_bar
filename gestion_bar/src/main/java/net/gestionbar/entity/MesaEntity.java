package net.gestionbar.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "mesa")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MesaEntity implements Serializable, GenericEntityInterface {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	private Long numero;
	@ManyToOne(fetch=FetchType.EAGER )
	@JoinColumn(name="local_id")
	private LocalEntity local;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public LocalEntity getLocal() {
		return local;
	}
	public void setLocal(LocalEntity local) {
		this.local = local;
	}
	public MesaEntity(Long id, Long numero, LocalEntity local) {
		super();
		this.id = id;
		this.numero = numero;
		this.local = local;
	}
	public MesaEntity(Long numero, LocalEntity local) {
		super();
		this.numero = numero;
		this.local = local;
	}
	public MesaEntity() {
		super();
	}
	@Override
	public String toString() {
		return "MesaEntity [id=" + id + ", numero=" + numero + ", local=" + local + "]";
	}
		
	
}
