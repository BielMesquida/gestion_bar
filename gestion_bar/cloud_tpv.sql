-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 02-02-2020 a las 23:18:33
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cloud_tpv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea_pedido`
--

CREATE TABLE `linea_pedido` (
  `id` bigint(20) NOT NULL,
  `cantidad` bigint(45) DEFAULT NULL,
  `ticket_id` bigint(20) NOT NULL,
  `producto_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `linea_pedido`
--

INSERT INTO `linea_pedido` (`id`, `cantidad`, `ticket_id`, `producto_id`) VALUES
(1, 28, 1, 1),
(2, 2, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

CREATE TABLE `local` (
  `id` bigint(20) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `nif` varchar(45) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`id`, `descripcion`, `login`, `password`, `nif`, `telefono`, `email`) VALUES
(1, 'BielMesquida', 'biel', 'biel', '41747065h', 681555477, 'biel@cloudtpv.com'),
(2, 'Pastisseria  Maria', 'Ramon8311', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '19541196O', 615980935, 'Ramon8311@cloudtpv.com'),
(3, 'Bar Maria', 'Paco4941', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '32133035O', 277996931, 'Paco4941@cloudtpv.com'),
(4, 'Restaurante  Ramon', 'Ramon5964', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '46902003O', 109939784, 'Ramon5964@cloudtpv.com'),
(5, 'Pastisseria  Paquita', 'Maria4793', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '45643603O', 621521131, 'Maria4793@cloudtpv.com'),
(6, 'Restaurante  Paquita', 'Toni1490', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '95782433O', 307105310, 'Toni1490@cloudtpv.com'),
(7, 'Restaurante  Maria', 'Paquita2380', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '84073104O', 251749866, 'Paquita2380@cloudtpv.com'),
(8, 'Bar Paquita', 'Ramon2593', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '95600994O', 142871718, 'Ramon2593@cloudtpv.com'),
(9, 'Pastisseria  Paquita', 'Ramon6593', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '23212916O', 655536610, 'Ramon6593@cloudtpv.com'),
(10, 'Bar Paquita', 'Paquita2176', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '41732694O', 120699108, 'Paquita2176@cloudtpv.com'),
(11, 'Bar Ramon', 'Toni2584', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '76301279O', 928057022, 'Toni2584@cloudtpv.com'),
(12, 'Pastisseria  Ramon', 'Toni7833', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '73900245O', 231867778, 'Toni7833@cloudtpv.com'),
(13, 'Restaurante  Paquita', 'Toni6833', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '58292497O', 774247371, 'Toni6833@cloudtpv.com'),
(14, 'Pastisseria  Maria', 'Paco9382', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '37626033O', 572156162, 'Paco9382@cloudtpv.com'),
(15, 'Restaurante  Paquita', 'Ramon4563', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '46632674O', 261264064, 'Ramon4563@cloudtpv.com'),
(16, 'Pastisseria  Ramon', 'Paquita7338', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '70080950O', 534225445, 'Paquita7338@cloudtpv.com'),
(17, 'Restaurante  Ramon', 'Paquita8606', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '88738646O', 901927873, 'Paquita8606@cloudtpv.com'),
(18, 'Pastisseria  Paquita', 'Ramon2965', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '60781772O', 921156103, 'Ramon2965@cloudtpv.com'),
(19, 'Bar Maria', 'Maria7124', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '23242237O', 532195512, 'Maria7124@cloudtpv.com'),
(20, 'Restaurante  Maria', 'Ramon2551', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '19858917O', 974380287, 'Ramon2551@cloudtpv.com'),
(21, 'Restaurante  Ramon', 'Toni7237', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '60998889O', 450620374, 'Toni7237@cloudtpv.com'),
(22, 'Bar Maria', 'Ramon9039', 'da8ab09ab4889c6208116a675cad0b13e335943bd7fc418782d054b32fdfba04', '69466185O', 721274611, 'Ramon9039@cloudtpv.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id` bigint(20) NOT NULL,
  `numero` bigint(20) DEFAULT NULL,
  `local_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id`, `numero`, `local_id`) VALUES
(1, 1, 3),
(2, 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` bigint(20) NOT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `existencias` bigint(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `tipo_producto_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `codigo`, `existencias`, `precio`, `imagen`, `descripcion`, `tipo_producto_id`) VALUES
(1, '123456', 1, 1.25, 'link a una imagen', 'cocacola', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id` bigint(20) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `total` double DEFAULT NULL,
  `mesa_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id`, `fecha`, `total`, `mesa_id`) VALUES
(1, '2020-01-17 00:00:00', 27.55, 2),
(2, '2020-01-31 17:17:38', 87.21, 1),
(3, '2020-01-18 00:00:00', 27.55, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `id` bigint(20) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_producto`
--

INSERT INTO `tipo_producto` (`id`, `descripcion`) VALUES
(1, 'bebida'),
(2, 'comida');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `linea_pedido`
--
ALTER TABLE `linea_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_linea_pedido_ticket1_idx` (`ticket_id`),
  ADD KEY `fk_linea_pedido_producto1_idx` (`producto_id`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mesa_local1_idx` (`local_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producto_tipo_producto_idx` (`tipo_producto_id`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ticket_mesa1_idx` (`mesa_id`);

--
-- Indices de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `linea_pedido`
--
ALTER TABLE `linea_pedido`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `local`
--
ALTER TABLE `local`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `linea_pedido`
--
ALTER TABLE `linea_pedido`
  ADD CONSTRAINT `fk_linea_pedido_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `fk_linea_pedido_ticket1` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`);

--
-- Filtros para la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD CONSTRAINT `fk_mesa_local1` FOREIGN KEY (`local_id`) REFERENCES `local` (`id`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_tipo_producto` FOREIGN KEY (`tipo_producto_id`) REFERENCES `tipo_producto` (`id`);

--
-- Filtros para la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `fk_ticket_mesa1` FOREIGN KEY (`mesa_id`) REFERENCES `mesa` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
